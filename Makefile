# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/11 12:17:38 by dfarhi            #+#    #+#              #
#    Updated: 2021/11/26 00:25:07 by dfarhi           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CLIENT_FILES	= client.c minitalk_utils.c
CLIENT_OBJ		= ${CLIENT_FILES:.c=.o}

SERVER_FILES	= server.c minitalk_utils.c
SERVER_OBJ		= ${SERVER_FILES:.c=.o}

CC			= gcc -Wall -Wextra -Werror

AR			= ar rcs

INCLUDES	= -I. -I./libft/includes

LIB			= -L./libft/ -lft
LIBFT		= libft/libft.a

OPT_DEF		=

.c.o:
			${CC} -c ${INCLUDES} $< -o ${<:.c=.o} ${OPT_DEF}

all:		${LIBFT} client server

client:		${LIBFT} ${CLIENT_OBJ}
			${CC} ${INCLUDES} -o client ${CLIENT_OBJ} ${OPT_DEF} ${LIB}

server:		${LIBFT} ${SERVER_OBJ}
			${CC} ${INCLUDES} -o server ${SERVER_OBJ} ${OPT_DEF} ${LIB}

${LIBFT}:
			$(MAKE) -C ./libft ft_printf

confirm:	OPT_DEF= -D PRINT_CONFIRMATION=1
confirm:	fclean all

clean:
			rm -f ${CLIENT_OBJ} ${SERVER_OBJ}
			make -C ./libft clean

# "Force clean" remove all compiled files
fclean:		clean
			make -C ./libft fclean

# Rule to recompile
re:			fclean all

# Rules that do not have files, if those files exist, they are ignored
.PHONY:		all clean fclean re confirm
