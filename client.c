/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 21:30:28 by davifah           #+#    #+#             */
/*   Updated: 2021/11/25 16:25:39 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

static void	set_global(int n, siginfo_t *info, void *unused);
int	g_receive = -1;

// 0.046 seconds (46ms) for 100 characters
int	main(int ac, char **av)
{
	pid_t	pid;
	int		i;

	if (ac < 3)
		ft_printf("Not enough arguments given, quitting...\n");
	else
	{
		listen_signal(SIGUSR1, set_global, 0);
		listen_signal(SIGUSR2, set_global, 0);
		pid = (pid_t)ft_atoi(av[1]);
		i = -1;
		while (pid)
		{
			send_byte(av[2][++i], pid);
			if (!av[2][i])
				pid = 0;
		}
		if (PRINT_CONFIRMATION)
			ft_putchar_fd('\n', 1);
	}
}

static void	set_global(int n, siginfo_t *info, void *unused)
{
	(void)info;
	(void)unused;
	if (n == SIGUSR1)
		g_receive = 0;
	else if (n == SIGUSR2)
		g_receive = 1;
}
