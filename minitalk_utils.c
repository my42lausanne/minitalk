/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 22:03:24 by davifah           #+#    #+#             */
/*   Updated: 2021/11/26 00:13:38 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

int	send_byte(char c, pid_t pid)
{
	int		i;
	int		res;
	int		sent_bit;
	char	sent_c;

	res = 1;
	i = 8;
	sent_c = 0;
	while (--i >= 0 && res)
	{
		sent_bit = send_bit(c / ft_power(2, i), pid);
		if (sent_bit < 0)
			res = 0;
		sent_c += sent_bit;
		if (i)
			sent_c <<= 1;
		usleep(50);
		c = c % ft_power(2, i);
	}
	if (PRINT_CONFIRMATION && res)
		ft_putchar_fd(sent_c, 1);
	return (res);
}

int	send_bit(int bit, pid_t pid)
{
	int	sig;

	if (!bit)
		sig = SIGUSR1;
	else
		sig = SIGUSR2;
	g_receive = -1;
	if (kill(pid, sig))
		return (-1);
	sig = -1;
	while (g_receive == -1 && ++sig <= 200)
		usleep(1);
	return (g_receive);
}

int	listen_signal(int sig, void (*h)(int, siginfo_t*, void*), int options)
{
	int					r;
	struct sigaction	s;

	s.sa_sigaction = h;
	sigemptyset (&s.sa_mask);
	s.sa_flags = options;
	r = sigaction (sig, &s, NULL);
	return (r);
}
