/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 21:12:11 by davifah           #+#    #+#             */
/*   Updated: 2021/11/25 16:07:44 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINITALK_H
# define MINITALK_H

# include "expanded.h"
# include "ft_printf.h"

# ifndef PRINT_CONFIRMATION
#  define PRINT_CONFIRMATION 0
# endif

# include <signal.h>
# include <unistd.h>

extern int	g_receive;

int		send_byte(char c, pid_t pid);
int		send_bit(int bit, pid_t pid);
void	received_signal(int n, siginfo_t *info, void *unused);
int		listen_signal(int sig, void (*h)(int, siginfo_t*, void*), int options);
char	receive_byte(void);

#endif
