/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 21:15:13 by davifah           #+#    #+#             */
/*   Updated: 2021/11/26 00:19:30 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

int	g_receive = -1;

int	main(void)
{
	char	c;

	ft_printf("Server PID: %u\n", getpid());
	listen_signal(SIGUSR1, received_signal, SA_SIGINFO);
	listen_signal(SIGUSR2, received_signal, SA_SIGINFO);
	while (1)
	{
		c = receive_byte();
		if (!c)
			ft_putchar_fd('\n', 1);
		else if (ft_isprint(c))
			ft_putchar_fd(c, 1);
	}
}

char	receive_byte(void)
{
	char	c;
	int		count;

	c = 0;
	count = 0;
	while (1)
	{
		while (g_receive == -1)
			continue ;
		c += g_receive;
		g_receive = -1;
		if (++count == 8)
			return (c);
		else
			c <<= 1;
	}
}

void	received_signal(int n, siginfo_t *info, void *unused)
{
	(void)unused;
	if (n == SIGUSR1)
		g_receive = 0;
	else if (n == SIGUSR2)
		g_receive = 1;
	kill(info->si_pid, n);
}
